<?php
/*
Plugin Name: NT SlidesJS & Shortcodes
Plugin URI: http://plugins.nucleartuxedo.com/nt-slidesjs-shortcodes/
Description: Loads in SlidesJS and allows you to wrap content in shortcodes to produce slideshows.
Author: Milo Jennings
Version: 0.4.3
Author URI: http://nucleartuxedo.com/
Bitbucket Plugin URI: https://bitbucket.org/nucleartuxedo/nt-slidesjs
Bitbucket Branch: master
*/

/* Load in SlidesJS & jQuery easing*/

add_action('init', 'register_nt_slide_scripts');
add_action('wp_footer', 'print_nt_slide_scripts');

function register_nt_slide_scripts() { 
	wp_register_script("jquery-easing", WP_PLUGIN_URL ."/nt-slidesjs/js/jquery.easing.1.3.min.js", array("jquery"), false, true);
	wp_register_script("slidesJS", WP_PLUGIN_URL ."/nt-slidesjs/js/slides.min.jquery.js", array("jquery"), "1.1.9", true);
}
function print_nt_slide_scripts() {
	global $nt_slides_scripts_printed;
	if ( ! $nt_slides_scripts_printed )
			return;
	
	wp_print_scripts('jquery-easing');
	wp_print_scripts('slidesJS');
}

function nt_slides_filter_shortcode_content( $content ) {
    /* Parse nested shortcodes and add formatting. */
    $content = trim( wpautop( do_shortcode( $content ) ) );
    /* Remove '</p>' from the start of the string. */
    if ( substr( $content, 0, 4 ) == '</p>' )
        $content = substr( $content, 4 );
    /* Remove '<p>' from the end of the string. */
    if ( substr( $content, -3, 3 ) == '<p>' )
        $content = substr( $content, 0, -3 );
    /* Remove any instances of '<p></p>'. */
    $content = str_replace( array( '<p></p>' ), '', $content );
    return $content;
}

if ( ! function_exists( 'nt_delete_htmltags' ) ){
	function nt_delete_htmltags($content,$paragraph_tag=false,$br_tag=false){	
		#$content = preg_replace('#<\/p>(\s)*<p>$|^<\/p>(\s)*<p>#', '', trim($content));
		$content = preg_replace('#^<\/p>|^<br \/>|<p>$#', '', $content);
		$content = preg_replace('#<br \/>#', '', $content);
		$content = str_replace('&nbsp;', '', $content);
		if ( $paragraph_tag ) $content = preg_replace('#<p>|</p>#', '', $content);
		return trim($content);
	}
}
if ( ! function_exists( 'nt_content_helper' ) ){
	function nt_content_helper($content,$paragraph_tag=true,$br_tag=false){
		return nt_delete_htmltags( do_shortcode(shortcode_unautop($content)), $paragraph_tag, $br_tag );
	}
}

// Returns and array of the img tags
function grabImageUrls($htmlVar){
	if(!function_exists('file_get_html')){
		include(dirname(__FILE__) . '/simple_html_dom.php');
	}
	
	$doc = new DOMDocument();
	$doc->recover = true;
	$doc->strictErrorChecking = false;
	@$doc->loadHTML( $htmlVar );
	$xml = simplexml_import_dom( $doc);
	$images = $xml->xpath('//img');

	//access attributes like so:
	//echo $images[0]['src']
	//echo $images[1]['width']
	return $images;
}

/* Grab all img tags from within the contents of a shortcode and produce a slideshow */
/* uses the first image's height for the height of the slideshow */
add_shortcode("ntslideshow", "nt_slides_shortcode");
function nt_slides_shortcode($atts, $content = null) {
	global $nt_slides_scripts_printed;
	$nt_slides_scripts_printed = true;
	
	// increment $nt_slides_num var if more than 1 slideshow exists on a single page
	global $nt_slides_num;
	
	$nt_slides_instance_num = ( ( $nt_slides_num ) ? '-' . esc_attr( $nt_slides_num ) : '' );
	
	if($content == ''){
		return;
	}
	
	$nt_total_slides_num = substr_count($content, "[ntslide");
	
	// check if using custom slide divs
	if ( strpos($content, '[ntslide]') || strpos($content, '[ntslide ') ) { 
		$customSlideDivs = true;
	}
	
	$content = nt_content_helper( $content );
	$urlListArray = grabImageUrls( $content );
	extract(shortcode_atts(array(
		'class' => '',
		'style' => '',
		//slidesJS default parameters
 		'generatenextprev' => 'false',
		'generatepagination' => 'false',
		'paginationclass' => 'pagination',
		'pagination' => 'false',
		'paginationwrapper' => 'false',
		'previouslabel' => 'Previous',
		'nextlabel' => 'Next',
		'arrowwrapperclass' => 'arrow-nav',
		'arrowwrapper' => 'false',
		'arrowsinpagination' => 'false',
		'container' => 'nt_slides_container',
		'fadespeed' => 'false',
		'slidespeed' => 'false',
		'speed' => '800',
		'fadeeasing' => 'easeInOutQuad',
		'slideeasing' => 'easeInOutQuad',
		'effect' => 'fade',
		'crossfade' => 'false',
		'play' => '4000',
		'randomize' => 'false',
		'hoverpause' => 'false',
		'autoheight' => 'false',
		'autoheightspeed' => '350',
		'bigtarget' => 'false',
		'preload' => 'false',
		'width' => 'false',
		'height' => 'false',
		'wrapperclass' => 'nt_slides',
		'wrapperwidth' => 'false',
		'wrapperheight' => 'false'
	), $atts));
	
	// Add a increment to the wrapper class if there is more than 1 slideshow
	// and if a unique wrapper class isn't specified.
	if($wrapperclass == 'nt_slides'){
		$wrapperclass = ( ( $nt_slides_num ) ? $wrapperclass . '-' . esc_attr( $nt_slides_num ) : $wrapperclass );
	}
	
	$output = 
"<script type='text/javascript'>
jQuery(function($){
	$('div.{$wrapperclass}').slides({
		container: '{$container}',";
		
		if($arrowwrapper == 'true'){
			$generatenextprev = "false";
		}
			
		$output.= "generateNextPrev: {$generatenextprev},";
		
		if($paginationwrapper == 'true'){
			$generatepagination = "false";
		}
		
		$output.= "generatePagination: {$generatepagination},";
		
		// if pagination class is set, turn pagination on
		if($paginationclass !== 'pagination'){
			$pagination = 'true';
		}
		
		// If pagination generation is true, also enable pagination
		// since the default is disabled, and you need both to be true.
		if($generatepagination == 'true' || $paginationwrapper !== 'false'){
			$output.= "\n\t\tpagination: 'true',";
			$pagination = 'true';
		} else {
			// allow pagination to be turned on even if it's not being generated
			// this allows custom pagination
			$output.= "\n\t\tpagination: {$pagination},";
		}
		
		// if pagination is false, randomize pagination identifier
		// this prevents conflicts between multiple slideshows
		
		if($pagination == 'false'){
			$output.= "\n\t\tpaginationClass: 'pagination".rand()."',";
		} else {
			$output.= "\n\t\tpaginationClass: '{$paginationclass}',";
		}
		
		$output.= "
		fadeEasing: '{$fadeeasing}',
		slideEasing: '{$slideeasing}',
		effect: '{$effect}',";
		if($effect == "slide"){
			if($slidespeed !== 'false') {
				$output.= "\n\t\tslideSpeed: {$slidespeed},";
			} else {
				$output.= "\n\t\tslideSpeed: {$speed},";
			}
		} else {
			if($fadespeed !== 'false') {
				$output.= "\n\t\tfadeSpeed: {$fadespeed},";
			} else {
				$output.= "\n\t\tfadeSpeed: {$speed},";
			}
		}
		$output.= "
		crossfade: {$crossfade},
		play: {$play},
		randomize: {$randomize},
		hoverPause: {$hoverpause},
		autoHeight: {$autoheight},
		autoHeightSpeed: {$autoheightspeed},
		bigTarget: {$bigtarget},";
		
		if($autoheight == 'true'){
			$output.= "\n\t\tpreload: true,";
		} else{
			$output.= "\n\t\tpreload: {$preload},";
		}
		$output.= "
		preloadImage: '".WP_PLUGIN_URL."/nt-slidesjs/images/ajax-loader-on-black.gif'
	});
});
</script>
";
	//acquire slideshow size from images if not manually specified
	$slideshowWidth = ($width == 'false' ? $urlListArray[0]['width'] : $width );
	$slideshowHeight = ($height == 'false' ? $urlListArray[0]['height'] : "height: " . $height );
	
	$output.= "<div class='" . $class . " " . $wrapperclass . " nt-slides slides";
	if($autoheight == 'true')
		$output.= " autoHeightActive";
		
	if($pagination == 'true')
		$output.= " paginationActive";
		
	$output.= "' style='" . $style;
	if($wrapperwidth == 'false'){
		$output.= ($width == 'false' ? "width: " . $slideshowWidth . "px;" : " width: " . $width . ";");
	} else {
		$output.= "width: " . $wrapperwidth . "; ";
	}
	if($autoheight == 'false'){
		if($wrapperheight == 'false'){
			$output.= ($height == 'false' ? " height: " . $slideshowHeight . "px;" : " height: " . $height . ";");
		} else {
			$output.= " height: " . $wrapperheight . ";";
		}
	}
	$output.= "'>";
	$output.= "<div class='" . $container . "' style='";
	//$output.= ($width == 'false' ? "width: " . $slideshowWidth . "px; " : "width: " . $width . "; ");
	$output.= $slideshowWidth . ";";
	if($autoheight == 'false'){
		$output.= ($height == 'false' ? "height: " . $slideshowHeight . "px; " : "height: " . $height . ";");
	} 
	$output.= "'>";
	
	
	if ( $customSlideDivs ) { 
		$output.= nt_content_helper($content);
	} else {
		//insert slides of images
		$nt_total_slides_num = count($urlListArray);
		$i = 1;
		foreach ($urlListArray as $img) {
			//$output.= "<div class='slide slide". $i ."'><img alt=' ". $img['alt'] ."' width='". $img['width'] ."'  src='". $img['src'] ."' /></div>";
			$output.= "<div class='slide slide". $i ."' style='";
			$output.= ($height == 'false' ? "height: " . $img['height'] . "px; " : "height: " . $height . ";");
			//$output.= "height: ". $img['height'] ."px";
			$output.= "'><img width='". $img['width'] ."' height='". $img['height'] ."' alt=' ". $img['alt'] ."' src='". $img['src'] ."' /></div>";
			$i++;
		}
	}
	//$output.= print_r($urlList, true);
	$output.= "</div></div>";
	
	//$output.= "number of slides = " . $nt_total_slides_num;
	
	$arrowNavMarkup = "<div class='{$arrowwrapperclass}'>";
	$arrowNavMarkup .= "<a class='prev' href='#'>{$previouslabel}</a>";
	$arrowNavMarkup .= "<a class='next' href='#'>{$nextlabel}</a>";
	$arrowNavMarkup .= "</div>";
	
	if($paginationwrapper !== 'false' && $generatepagination == 'false'){
		$output .= "<div class='pagination-wrapper'>";
		if($arrowsinpagination == "true"){
			$output .= $arrowNavMarkup;
		}
		$output .= "<ul class='{$paginationclass}'>";
		$i = 1;
		for ( $nt_total_slides_num; $i <= $nt_total_slides_num; $i++) {
			$pagination_num = $i-1;
			$output .=  "<li><a href='#{$pagination_num}'></a></li>";
		}
		$output .= "</ul>";
		$output .= "</div>";
	}
	
	if($arrowwrapper == 'true' && $arrowsinpagination == 'false'){
		$output .= $arrowNavMarkup;
	}
	
	global $nt_prev_slides_num;
	$nt_prev_slides_num = $nt_slides_num;
	$nt_slides_num++;
	
	return $output;
}


add_shortcode("ntslide", "nt_slide_shortcode");
function nt_slide_shortcode($atts, $content = null){
	global $nt_slides_num;
	global $nt_prev_slides_num;
	global $nt_single_slide_num;
	
	//if processing a slide in the same slideshow, do nothing
	//otherwise, reset to 1
	
	if($nt_slides_num != $nt_prev_slides_num){
		$nt_prev_slides_num = $nt_slides_num;
		$nt_single_slide_num = 1;
	}
	
	// echo "<p>nt_slides_num: {$nt_slides_num} - nt_prev_slides_num: {$nt_prev_slides_num} - nt_single_slide_num: {$nt_single_slide_num}</p>";
	
	// if an intial slide number hasn't been set, reset to 1
	if(!$nt_single_slide_num){$nt_single_slide_num = 1;}
	
	extract(shortcode_atts(array(
		"width" => 'false',
		"height" => 'false',
		"id" => '',
		"class" => '',
		"autosize" => 'false'
	), $atts));
	
	$content = nt_content_helper($content);
	
	$id = ($id <> '') ? " id='" . esc_attr( $id ) . "'" : '';
	$class = ($class <> '') ? esc_attr( ' ' . $class ) : '';
	
	if($autosize == 'true'){
		//acquire slides size from images if not specified
		$urlListArray = grabImageUrls( $content );
		$slideWidth = $urlListArray[0]['width'];
		$slideHeight = $urlListArray[0]['height'];
	}
	
	$output = "<div{$id} class='nt-slide {$class} slide{$nt_single_slide_num}' style='";
	
	if($autosize == 'true'){
		$output.= "width: " . $slideWidth . ";";
		$output.= "height: " . $slideHeight . ";";
	} else {
		$output.= ($width == 'false' ? "" : "width: " . $width . "; ");
		$output.= ($height == 'false' ? "" : "height: " . $height . ";");
	}
	
	$output.= "'>{$content}</div>";
	

	$nt_single_slide_num++;
		
	return $output;
}