# NT SlidesJS & Shortcodes
Contributors: Milo Jennings
Tags: 
Requires at least: 3.0
Tested up to: 4.3
Version: 0.4.2
Stable tag: 0.4.2

Loads in SlidesJS and allows you to wrap content in shortcodes to produce slideshows. For use anywhere shortcodes are accepted. The plugin will output a slideshow from any normally formatted HTML img tags within the shortcode.

## ntslideshow shortcode

```html
[ntslideshow fadeSpeed="800" effect="fade" play="4000"]
<img src="http://site.com/image1.jpg">
<img src="http://site.com/image2.jpg">
<img src="http://site.com/image3.jpg">
[/ntslideshow]
```

### Extra arguments added to plugin:

 - width: string (example: 200px or 100%)
 - height: string (example: 200px or 100%)
 - wrapperclass: string (default: "slides")
 - wrapperwidth: string (default: false)
 - wrapperheight: string (default: false)
 - speed: integer (default: 800)
 - paginationWrapper: string (default: false)
 - paginationWrapperClass: string (default: false)
 - previousLabel: string (default: "Previous")
 - nextLabel: string (default: "Next")
 - arrowWrapper: string (default: false)
 - arrowWrapperClass: string (default: "arrow-nav")
 - arrowsInPagination: string (default: "false")

### Default arguments for slidesJS:

 - generatenextprev: false
 - generatepagination: false
 - paginationclass: pagination
 - pagination: true
 - container: nt_slides_container
 - fadeeasing: easeInOutQuad
 - slideeasing: easeInOutQuad
 - fadespeed: deprecated (use speed)
 - slidespeed: deprecated (use speed)
 - effect: fade
 - crossfade: false
 - play: 4000
 - randomize: false
 - hoverpause: false
 - autoheight: false
 - autoheightspeed: 350
 - bigtarget: false
 - preload: false

## ntslide shortcode
You can also create slides with custom content, rather than simple image slideshows.

```html
[ntslideshow fadeSpeed="800" generatepagination=true generatenextprev=true]

[ntslide]
<img src="http://site.com/image1.jpg">
<a href="http://google.com">google</a>
[/ntslide]

[ntslide]
<img src="http://site.com/image2.jpg">
<a href="http://google.com">google</a>
[/ntslide]

[/ntslideshow]
```

### available arguments for [ntslide]
 - width
 - height
 - id
 - class
 - autosize (if the shortcode contains an image, it can set the slide to the size of that image)