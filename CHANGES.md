# Changelog

## 0.4.3 [12/01/2015]

- Added github autoupdater support
- Renamed `index.php` to `nt-slidesjs.php`

## 0.4.2 [08/27/2012]

 - HTML characters are now encoded when passed through DOM parser
 - Added class to slideshow wrapper when pagination is active ("paginationActive").
 - Added option to use custom. And along with it 3 new arguments, "paginationWrapper", "paginationWrapperClass", "previousLabel", "nextLabel",  "arrowWrapper",  "arrowWrapperClass", "arrowsInPagination".
 - Next & previous navigation now function when markup appears outside of slideshow wrapper.

## 0.4.1 [08/10/2012]

 - Added conditional logic to only include javascript files if short code is used.
 - Added slide incrementing to slideshows slides that use the [nt_slide] short code, and not just the image only slideshows.
 - Added fallback to set images sizes on image only slideshows where images have no width or height attribute assigned.
 - Slide containers now have a height set on each .slide div if auto height is enabled. This number is based on the height attribute of the image tag.
 - Edited javascript functions to allow pagination outside the slideshow container.
 - Now automatically sets to "pagination: true", if a custom pagination class is defined

## 0.4 [07/10/2012]

 - Fixed issue with unencoded ampersands being parsed by grabImageUrls(). Content first runs through htmlspecialchars() now.
 - Added "speed" options that auto determines if it needs to set fadeSpeed or slideSpeed.
 - Added [ntslide] shortcode for custom, individual slides

## 0.3 [03/20/2012]

 - Updated enqueueing method to latest Wordpress method
 - Added "wrapperclass" option
 - Added "pagination" option
 - Added "container" option
 - Added more documentation
 - Added self-hosted versioning code

## 0.2 [01/30/2012]

 - Added ability to set the width and height of slideshow manually using 'width' and 'height' arguments to shortcode.

## 0.1 [12/28/2011]

 - Initial Commit
 - Added self hosting code
 - Updated slideJS from 1.1.4 to 1.1.9
 - Switched from dev JS to minified JS
